#!/bin/bash

template="H2_base.in"
repstr="xxxx"

# Again we set an initial value for the x-coordinate of on H-atom and a delta
# value as variables.
hx1=1.10
dhx=0.01

# Empty the file, since we'll be appending to it in the calculation loop.
> etot_v_bl.dat

for i in {00..100..1}
do
  inp="H2_${i}.in"
  # We save the output filename to a variable also.
  out="${inp%.*}.out"

  # Again we use bc to get the atomic position for each input.
  hx=$(echo "$hx1 + $i * $dhx" | bc)
  sed "s/$repstr/$hx/" $template > $inp
  pw.x < $inp &> $out

  # awk is inside the loop this time, and we are appending to the data file
  # after each calculation completes.
  awk -v bl=$hx '/^!.*total/{print bl, $5}' $out >> etot_v_bl.dat
done
